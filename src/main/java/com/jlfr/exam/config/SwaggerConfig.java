package com.jlfr.exam.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {
	
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("com.jlfr.exam.controller"))              
          .paths(PathSelectors.any())                          
          .build()
          .useDefaultResponseMessages(false)
          .apiInfo(apiInfo());
    }
	
	private ApiInfo apiInfo() {
	    return new ApiInfo(
	      "Luis Flores Exam", 
	      "Examen Api Rest", 
	      "1.0", 
	      "Terms of service", 
	      new Contact("Luis Flores", "", "n29frl@outlook.com"), 
	      "License of API", "API license URL", Collections.emptyList());
	}
}
