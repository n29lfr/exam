package com.jlfr.exam.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DaoConfig {
	
	@Bean	
	@Primary
	@ConfigurationProperties(prefix="spring.datasource.test")
	public DataSourceProperties aquilaDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean	
	@ConfigurationProperties(prefix="spring.datasource.test")
	@Primary
	@Qualifier("testDataSource")
	public DataSource aquilaDataSource() {
		return aquilaDataSourceProperties().initializeDataSourceBuilder().build();
	}
	
	
}
