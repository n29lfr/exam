package com.jlfr.exam.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jlfr.exam.dto.Item;
import com.jlfr.exam.dto.ItemUpdated;
import com.jlfr.exam.ex.NoDataFoundException;
import com.jlfr.exam.service.ItemService;

@RestController
@Validated
public class ItemsController {
	
	private static final Logger LOG = LoggerFactory.getLogger(ItemsController.class);
	
	@Autowired
	private ItemService itemService;
	
	@GetMapping(value = "/items/{id}")
	public ResponseEntity<Item> getItem(@Valid @PathVariable @Size(min = 10, max = 10, message = "El ID no cumple el formato requerido") String id) throws NoDataFoundException {		
		
		return new ResponseEntity<Item>(this.itemService.getItem(id), HttpStatus.OK);
	}
	
	@GetMapping("/items")
	public ResponseEntity<List<Item>> getItems() throws NoDataFoundException {
		return new ResponseEntity<List<Item>>(this.itemService.getItems(), HttpStatus.OK);		
	}
	
	@PostMapping("/items")
	public ResponseEntity<Item> updateItem(@Valid @RequestBody ItemUpdated item) throws NoDataFoundException {		
		return new ResponseEntity<Item>(this.itemService.updateItem(item), HttpStatus.OK);
	}

}
