package com.jlfr.exam.ex;

public class ExamException extends RuntimeException {

    public ExamException(String message) {
        super(message);
    }
}
