package com.jlfr.exam.dto;

import java.math.BigDecimal;

public class Item {
	
	private String id;
	private String name;
	private String description;
	private String model;
	private BigDecimal price;
	
	public Item() {
		
	}
	
	public Item(String id, String name, String description, String model, BigDecimal price) {	
		this.id = id;
		this.name = name;
		this.description = description;
		this.model = model;
		this.price = price;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", name=" + name + ", description=" + description + ", model=" + model + ", price="
				+ price + "]";
	}

}
