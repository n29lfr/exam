package com.jlfr.exam.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ItemUpdated {

	@NotNull(message = "ID requerido")
	@NotBlank(message = "ID requerido")
	@Size(min = 10, max = 10, message = "El ID no cumple el formato requerido")
	private String id;
	
	@Size(min = 1, max = 200, message = "La descripcion no cumple el formato requerido")
	private String description;
	
	@Size(min = 10, max = 10, message = "El modelo no cumple el formato requerido")
	private String model;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "ItemUpdated [id=" + id + ", description=" + description + ", model=" + model + "]";
	}
	

}
