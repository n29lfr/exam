package com.jlfr.exam.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class RutasDao {

	@Autowired
	private DataSource dataSource;
	
	//@EventListener(ApplicationReadyEvent.class)
	public void insert() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		
		String sql = "insert into file_type_rutas( " + 
				"  id_ruta, " + 
				"  valor, " + 
				"  fecha_alta, " + 
				"  fecha_modifica, " + 
				"  id_usuario_alta, " + 
				"  id_usuario_modifica) values (2,'este es el valor', ('2021-03-22 07:00:00')::timestamp, ('2021-03-22 07:01:00')::timestamp, -1, -1); ";
		
		jdbcTemplate.execute(sql);
	}
	
}
