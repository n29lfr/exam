package com.jlfr.exam.dao;

import java.util.List;

import com.jlfr.exam.dto.Item;
import com.jlfr.exam.dto.ItemUpdated;
import com.jlfr.exam.ex.ExamException;

public interface ItemDao {
	
	public List<Item> getItem(String id) throws ExamException;
	public List<Item> getItems() throws ExamException;
	public List<Item> updateItem(ItemUpdated item) throws ExamException; 

}
