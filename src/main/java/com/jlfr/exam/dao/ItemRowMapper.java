package com.jlfr.exam.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.jlfr.exam.dto.Item;

public class ItemRowMapper implements RowMapper<Item> {

	@Override
	public Item mapRow(ResultSet rs, int rowNum) throws SQLException {		
		
		return new Item(rs.getString("id"), 
				rs.getString("name"), 
				rs.getString("description"), 
				rs.getString("model"), 
				rs.getBigDecimal("price"));
	}

}
