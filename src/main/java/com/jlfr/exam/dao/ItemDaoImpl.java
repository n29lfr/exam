package com.jlfr.exam.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.jlfr.exam.dto.Item;
import com.jlfr.exam.dto.ItemUpdated;
import com.jlfr.exam.ex.ExamException;

@Repository
public class ItemDaoImpl implements ItemDao {
	
	private static final Logger LOG = LoggerFactory.getLogger(ItemDaoImpl.class);
	
	@Autowired
	private DataSource dataSource;
	
	private final String SCHEMA = "exam";
	private final String SPCON_ITEM = "fnsel_item";
	private final String SPCON_ITEMS = "fnsel_items";
	private final String SPUPD_ITEM = "fnupd_item";
	
	private final String SP_PARAM_ID = "pc_id";
	private final String SP_PARAM_DESCRIPTION = "pc_description";
	private final String SP_PARAM_MODEL = "pc_model";
	private final String VRCURSOR = "vr_cursor";
	
	private SimpleJdbcCall spSelItem;
	private SimpleJdbcCall spSelItems;
	private SimpleJdbcCall spUpdItem;
	
	@PostConstruct
	public void init() {		
		
		this.spSelItem = new SimpleJdbcCall(this.dataSource)
				.withSchemaName(SCHEMA)
				.withProcedureName(SPCON_ITEM)
				.withoutProcedureColumnMetaDataAccess()
				.useInParameterNames(SP_PARAM_ID )
				.declareParameters(new SqlParameter(SP_PARAM_ID, Types.VARCHAR))				
				.declareParameters(new SqlOutParameter(VRCURSOR, Types.OTHER, new ItemRowMapper()));
		
		this.spSelItems = new SimpleJdbcCall(this.dataSource)
				.withSchemaName(SCHEMA)
				.withProcedureName(SPCON_ITEMS)
				.withoutProcedureColumnMetaDataAccess()												
				.declareParameters(new SqlOutParameter(VRCURSOR, Types.OTHER, new ItemRowMapper()));
		
		this.spUpdItem = new SimpleJdbcCall(this.dataSource)
				.withSchemaName(SCHEMA)
				.withProcedureName(SPUPD_ITEM)
				.withoutProcedureColumnMetaDataAccess()
				.useInParameterNames(SP_PARAM_ID)
				.useInParameterNames(SP_PARAM_DESCRIPTION)
				.useInParameterNames(SP_PARAM_MODEL)
				.declareParameters(new SqlParameter(SP_PARAM_ID, Types.VARCHAR))	
				.declareParameters(new SqlParameter(SP_PARAM_DESCRIPTION, Types.VARCHAR))	
				.declareParameters(new SqlParameter(SP_PARAM_MODEL, Types.VARCHAR))	
				.declareParameters(new SqlOutParameter(VRCURSOR, Types.OTHER, new ItemRowMapper()));
	}
	
	@Transactional
	public List<Item> getItem(String id) throws ExamException {
		
		try {
			
			SqlParameterSource in = new MapSqlParameterSource()
					.addValue(SP_PARAM_ID, id);					
						
						
			Map<String, Object> out = spSelItem.execute(in);
					
			@SuppressWarnings("unchecked")
			List<Item> lts = (List<Item>) out.get(VRCURSOR);
			
			return lts;
			
		} catch (Exception ex) {
			LOG.error("Error al consultar el ITEM con id {} ", id , ex);
			throw new ExamException("Error al consultar el ITEM con id " + id);
		}
		
	}
	
	@Transactional
	public List<Item> getItems() throws ExamException {
		
		try {							
						
			Map<String, Object> out = spSelItems.execute();
					
			@SuppressWarnings("unchecked")
			List<Item> lts = (List<Item>) out.get(VRCURSOR);
			
			return lts;
			
		} catch (Exception ex) {
			LOG.error("Error al consultar todos los ITEMS " , ex);
			throw new ExamException("Error al consultar todos los ITEMS");
		}
		
	}
	
	@Transactional
	public List<Item> updateItem(ItemUpdated item) throws ExamException {
		
		try {
			
			SqlParameterSource in = new MapSqlParameterSource()
					.addValue(SP_PARAM_ID, item.getId())
					.addValue(SP_PARAM_DESCRIPTION, item.getDescription())
					.addValue(SP_PARAM_MODEL, item.getModel());								
						
			Map<String, Object> out = spUpdItem.execute(in);
					
			@SuppressWarnings("unchecked")
			List<Item> lts = (List<Item>) out.get(VRCURSOR);
			
			return lts;
			
		} catch (Exception ex) {
			LOG.error("Error al actualizar el ITEM con id {} ", item.getId() , ex);
			throw new ExamException("Error al consultar el ITEM con id " + item.getId());
		}
		
	}

}
