package com.jlfr.exam.service;

import java.util.List;

import com.jlfr.exam.dto.Item;
import com.jlfr.exam.dto.ItemUpdated;
import com.jlfr.exam.ex.ExamException;
import com.jlfr.exam.ex.NoDataFoundException;

public interface ItemService {
	
	public Item getItem(String id) throws ExamException, NoDataFoundException;
	
	public List<Item> getItems() throws ExamException;
	
	public Item updateItem(ItemUpdated item) throws ExamException, NoDataFoundException;

}
