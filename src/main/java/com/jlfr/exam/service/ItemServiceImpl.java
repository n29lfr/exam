package com.jlfr.exam.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jlfr.exam.dao.ItemDao;
import com.jlfr.exam.dto.Item;
import com.jlfr.exam.dto.ItemUpdated;
import com.jlfr.exam.ex.ExamException;
import com.jlfr.exam.ex.NoDataFoundException;

@Service
public class ItemServiceImpl implements ItemService {

	private static final Logger LOG = LoggerFactory.getLogger(ItemServiceImpl.class);
	
	@Autowired
	private ItemDao itemDao;
	
	public Item getItem(String id) throws ExamException, NoDataFoundException {
		
		List<Item> items = itemDao.getItem(id);
		
		if (!items.isEmpty()) return items.get(0);
		 
		throw new NoDataFoundException("El ID no existe " + id);				
		
	}
	
	public List<Item> getItems() throws ExamException {
		
		return itemDao.getItems();
		
	}
	
	public Item updateItem(ItemUpdated item) throws ExamException, NoDataFoundException {
		
		List<Item> items = itemDao.updateItem(item);		
		
		if (!items.isEmpty()) return items.get(0);
		 
		throw new NoDataFoundException("El ID no existe " + item.getId());
	}
}
