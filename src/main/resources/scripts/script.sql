CREATE SCHEMA exam;

CREATE TABLE exam.items
(
  id character varying(10) NOT NULL,
  name character varying(20) NOT NULL,
  description character varying(200) NOT NULL,
  price numeric(16,2) NOT NULL,
  model character varying(10) NOT NULL,
  
  CONSTRAINT pkfitems PRIMARY KEY (id) 
);

COMMENT ON TABLE exam.items
  IS 'Table for store store items';

INSERT INTO exam.items(id, name, description, price, model)
	VALUES('MXN0000001','Articulo 1', 'Descripcion 1', 1.01, 'MOD0000001'),
		  ('MXN0000002','Articulo 2', 'Descripcion 2', 2.01, 'MOD0000002'),
		  ('MXN0000003','Articulo 3', 'Descripcion 3', 3.01, 'MOD0000003'),
		  ('MXN0000004','Articulo 4', 'Descripcion 4', 4.01, 'MOD0000004'),
		  ('MXN0000005','Articulo 5', 'Descripcion 5', 5.01, 'MOD0000005'),
		  ('MXN0000006','Articulo 6', 'Descripcion 6', 6.01, 'MOD0000006'),
		  ('MXN0000007','Articulo 7', 'Descripcion 7', 7.01, 'MOD0000007'),
		  ('MXN0000008','Articulo 8', 'Descripcion 8', 8.01, 'MOD0000008'),
		  ('MXN0000009','Articulo 9', 'Descripcion 9', 9.01, 'MOD0000009'),
		  ('MXN0000010','Articulo 10', 'Descripcion 10', 10, 'MOD0000010');
		  
		  
CREATE OR REPLACE FUNCTION exam.fnsel_items() 
	RETURNS refcursor AS $$	
	DECLARE 
		vr_cursor refcursor;
	BEGIN		
        OPEN vr_cursor FOR
			SELECT id, name, description, price, model FROM exam.items;
		
		return vr_cursor;
	END;$$
	LANGUAGE 'plpgsql';
	
CREATE OR REPLACE FUNCTION exam.fnsel_item(IN pc_id varchar) 
	RETURNS refcursor AS $$	
	DECLARE 
		vr_cursor refcursor;
	BEGIN		
        OPEN vr_cursor FOR
			SELECT id, name, description, price, model FROM exam.items WHERE id = pc_id;
		
		return vr_cursor;
	END;$$
	LANGUAGE 'plpgsql';
	
CREATE OR REPLACE FUNCTION exam.fnupd_item(IN pc_id varchar, IN pc_description varchar, IN pc_model varchar) 
	RETURNS refcursor AS $$	
	DECLARE 
		vr_cursor refcursor;
	BEGIN
		
		IF (pc_description IS null) AND (pc_model IS NOT null) then
        	UPDATE exam.items set model = pc_model WHERE id = pc_id;			
		ELSEIF (pc_description is not null AND pc_model is null) then
			UPDATE exam.items set description = pc_description WHERE id = pc_id;			
		ELSEIF (pc_description IS NOT null) AND (pc_model IS NOT null) then
			UPDATE exam.items set description = pc_description, model = pc_model WHERE id = pc_id;			
		END IF;
		
		OPEN vr_cursor FOR
		SELECT id, name, description, price, model FROM exam.items WHERE id = pc_id;
		return vr_cursor;
		
	END;$$
	LANGUAGE 'plpgsql';
	
	