PASOS PARA COMPILACION Y DESPLIEGUE

1.- Ejecutar el script de Base de datos Postgres en su SGBD
2.- En la consola CMD de su equipo windows ir a la carpeta del proyecto
3.- Si desean obtener el archivo ejecutable JAR pueden ejecutar el comando 'mvn clean package'
	3.1.- Se generara el archivo exam-1.0.0.jar en la carpeta target/ del proyecto
	3.2.- Ejecutar la instrucción java -jar target/exam-1.0.0.jar

4.- Si desean ejecutar el codigo sin el archivo jar ejecutar el comando 'mvn spring-boot:run'
5.- Para desplegar la documetacion del servicio dirijase en su navegador a la ruta 'http://localhost:8080/exam/v1/swagger-ui.html#'